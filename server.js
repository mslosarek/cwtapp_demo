var restify = require('restify'),
    server = restify.createServer()

require('./api_v1/app.js')(server);

server.listen(8080, function() {
    console.log('%s listening at %s', server.name, server.url);
});

server.get(/\/?.*/, restify.serveStatic({
    directory: './static',
    default: 'index.html'
}));