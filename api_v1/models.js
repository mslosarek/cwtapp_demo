var Sequelize = require('sequelize'),
    config = require('./config.js'),
    sequelize = new Sequelize(config.db, config.username, config.password, {
        dialect: 'mysql',
        host: config.host,
        port:    3306,
        define: {
            underscored: true
        }
    }),
    QuestionSet = sequelize.define('question_sets', {
        title: Sequelize.STRING,
        active: Sequelize.BOOLEAN,
    }, {
        timestamps: false,
        underscored: true
    }),
    QuestionSetPage = sequelize.define('question_set_pages', {
        title: Sequelize.STRING,
        short_title: Sequelize.STRING,
        description: Sequelize.STRING,
        sort: Sequelize.INTEGER
    }, {
        timestamps: false,
        underscored: true
    }),
    Question = sequelize.define('questions', {
        title: Sequelize.STRING,
        description: Sequelize.STRING,
        type: Sequelize.STRING,
        sort: Sequelize.INTEGER
    }, {
        timestamps: false,
        underscored: true
    }),
    QuestionOption = sequelize.define('question_options', {
        value: Sequelize.STRING,
        description: Sequelize.STRING,
        sort: Sequelize.INTEGER
    }, {
        timestamps: false,
        underscored: true
    }),
    Walkthrough = sequelize.define('walkthroughs', {
        name: Sequelize.STRING,
        teacher: Sequelize.STRING,
        subject: Sequelize.STRING,
        date: Sequelize.DATE,
        class_period: Sequelize.STRING
    }, {
        timestamps: true,
        underscored: true
    }),
    WalkthroughAnswer = sequelize.define('walkthrough_answers', {
        key: Sequelize.STRING,
        value: Sequelize.STRING,
        type: Sequelize.STRING
    }, {
        timestamps: true,
        underscored: true
    });


Walkthrough.hasMany(WalkthroughAnswer);
QuestionSet.hasMany(Walkthrough);
Walkthrough.belongsTo(QuestionSet);

QuestionSet.hasMany(QuestionSetPage);
QuestionSetPage.hasMany(Question);
Question.hasMany(QuestionOption);



sequelize
    .sync()
    .complete(function(err) {
        if (!!err) {
            console.log('An error occurred while creating the table:', err)
        } else {
            console.log('Sync Complete')
        }
    });

sequelize
    .authenticate()
        .complete(function(err) {
            if (!!err) {
                console.log('Unable to connect to the database:', err)
            } else {
                console.log('Connection has been established successfully.')
            }
        });

module.exports = {
    QuestionSet: QuestionSet,
    QuestionSetPage: QuestionSetPage,
    Question: Question,
    QuestionOption: QuestionOption,
    Walkthrough: Walkthrough,
    WalkthroughAnswer: WalkthroughAnswer
}