var Sequelize = require('sequelize'),
    restify = require('restify'),
    inflection = require('inflection'),
    models = require('./models.js');

module.exports = function(app) {

    app.use(restify.bodyParser());

    // a global get request for mode models
    // urls like /api/v1/question_sets
    app.get(/^\/api\/v1\/(question_sets|question_set_pages|questions|question_options)\/?$/, function(req, res, next) {
        var modelName = inflection.classify(req.params[0]),
            model;

        if (models[modelName]) {
            model = models[modelName];

            model.findAll().complete(function(err, records) {
                res.send(records);
                return next();  
            })
        } else {
            res.send(400, new Error('Bad Request'));
            return next();
        }
    });

    // get a specific question set
    // this will expand the selection to include related models
    app.get('/api/v1/question_sets/:question_set_id', function(req, res, next) {
        var question_set_id = req.params.question_set_id;
        if (question_set_id.match(/^[0-9]+$/)) {
            models.QuestionSet
                .find({
                    where: {id: question_set_id},
                    include: [{
                        model: models.QuestionSetPage, 
                        include: [{
                            model: models.Question,
                            include: [{
                                model: models.QuestionOption
                            }]
                        }]

                    }]
                })
                .success(function(question_set) {
                    res.send(question_set);
                    return next();
                });
        } else {
            res.send(400, new Error('Bad Request'));
            return next();
        }
    });


    // get a list of walkthroughs
    app.get('/api/v1/walkthroughs', function(req, res, next) {
        models.Walkthrough
            .findAll({
                include: [{
                    model: models.QuestionSet, 
                }]
            })
            .success(function(walkthroughs) {
                res.send(walkthroughs);
                return next();
            });    
    });

    // gets a specific walkthrough
    app.get('/api/v1/walkthroughs/:walkthrough_id', function(req, res, next) {
        var walkthrough_id = req.params.walkthrough_id;
        if (walkthrough_id.match(/^[0-9]+$/)) {
            models.Walkthrough
                .find({
                    where: {id: walkthrough_id},
                    include: [
                        {
                            model: models.WalkthroughAnswer, 
                        }
                    ]
                })
                .success(function(walkthrough) {
                    res.send(walkthrough);
                    return next();
                });
        } else {
            res.send(400, new Error('Bad Request'));
            return next();
        }
    });

    // utility functon to createOrUpdateWalkthrough
    function createOrUpdateWalkthrough(req, res, next) {
        function setWalkthroughAnswers(walkthrough) {
            models.WalkthroughAnswer.destroy({walkthrough_id: walkthrough.id})
                .success(function() {
                    // set the answers
                    var chainer = new Sequelize.Utils.QueryChainer;

                    req.params.walkthroughAnswers.forEach(function(walkthroughAnswer) {
                        walkthroughAnswer.walkthrough_id = walkthrough.id;
                        var walkthroughAnswer = models.WalkthroughAnswer.build(walkthroughAnswer);
                        chainer.add(walkthroughAnswer.save());
                    });

                    chainer
                        .run()
                        .success(function() {
                            models.Walkthrough
                                .find({
                                    where: {id: walkthrough.id},
                                    include: [{
                                        model: models.WalkthroughAnswer
                                    }]
                                })
                                .success(function(walkthrough) {
                                    res.send(walkthrough);
                                    return next();
                                });
                        });
                });
        }

        if (req.params.walkthrough_id && req.params.walkthrough_id.match(/^[0-9]+$/)) {
            models.Walkthrough
                .find(req.params.walkthrough_id)
                .success(function(walkthrough) {
                    walkthrough.updateAttributes({
                        name: req.params.name || null,
                        teacher: req.params.teacher || null,
                        subject: req.params.subject || null,
                        date: req.params.date || null,
                        class_period: req.params.class_period || null,
                        question_set_id: req.params.question_set_id || null
                    })
                    .success(setWalkthroughAnswers)
                    .error(function(err) {
                        res.send(new Error(err));
                    });
                });
        } else {
            models.Walkthrough
                .build({
                    name: req.params.name || null,
                    teacher: req.params.teacher || null,
                    subject: req.params.subject || null,
                    date: req.params.date || null,
                    class_period: req.params.class_period || null,
                    question_set_id: req.params.question_set_id || null
                })
                .save()
                .success(setWalkthroughAnswers)
                .error(function(err) {
                    res.send(new Error(err));
                });    
            }
        
    }

    // create a new walkthrough
    app.post('/api/v1/walkthroughs', createOrUpdateWalkthrough);

    // update an existing walkthrough
    app.put('/api/v1/walkthroughs/:walkthrough_id', createOrUpdateWalkthrough);
}