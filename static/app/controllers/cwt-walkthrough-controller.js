cwtapp.controller('cwtWalkthroughController', [
    '$scope', '$routeParams', 'cwtQuestionSetService', 'cwtWalkthroughService',
    function ($scope, $routeParams, cwtQuestionSetService, cwtWalkthroughService) {
        if ($routeParams.questionSetId && $routeParams.questionSetId.match(/^[0-9]+$/)) {
            var answersDirty = false,
                walkthroughId = false,
                questionSetId = $routeParams.questionSetId;

            $scope.activeTab = 'information';

            $scope.answers = {};

            $scope.informationTab = {
                short_title: 'Information',
                questions: [
                    {   
                        id: 'name',
                        title: 'Name',
                        type: 'text'
                    },
                    {
                        id: 'teacher',
                        title: 'Teacher',
                        type: 'text'
                    },
                    {
                        id: 'subject',
                        title: 'Subject',
                        type: 'text'
                    },
                    {
                        id: 'date',
                        title: 'Date',
                        type: 'date'
                    },
                    {
                        id: 'class_period',
                        title: 'Class Period',
                        type: 'text'
                    }
                ]
            }

            function loadAnswers(walkthrough) {
                if (walkthrough.id) {
                    walkthroughId = walkthrough.id;
                    answers = {};

                    for (i = 0; i < $scope.informationTab.questions.length; i++) {
                        answer = $scope.informationTab.questions[i];
                        answers[answer.id] = walkthrough[answer.id] || '';
                    }
                    for (i = 0; i < walkthrough.walkthroughAnswers.length; i++) {
                        answer = $scope.informationTab.questions[i];
                        if (walkthrough.walkthroughAnswers[i].type === 'boolean') {
                            answers[walkthrough.walkthroughAnswers[i].key] = !!parseInt(walkthrough.walkthroughAnswers[i].value);
                        } else {
                            answers[walkthrough.walkthroughAnswers[i].key] = walkthrough.walkthroughAnswers[i].value || '';
                        }
                    }
                    $scope.answers = answers;
                }
            }

            function saveAnswers() {
                var walkthrough = {
                        question_set_id: questionSetId,
                        walkthroughAnswers: []
                    },
                    i = 0,
                    answers = {},
                    answer;

                angular.copy($scope.answers, answers);

                for (i = 0; i < $scope.informationTab.questions.length; i++) {
                    answer = $scope.informationTab.questions[i];
                    walkthrough[answer.id] = answers[answer.id] || '';
                    delete answers[answer.id];
                }
                for (i in answers) {
                    if (answers.hasOwnProperty(i)) {
                        walkthrough['walkthroughAnswers'].push({
                            key: i,
                            value: answers[i],
                            type: (Object.prototype.toString.call(answers[i]) === '[object Boolean]' ? 'boolean' : 'string')
                        });
                    }
                }

                // check for a create or update
                console.log(walkthroughId);
                if (walkthroughId) {
                    // update
                    cwtWalkthroughService.update({walkthroughId: walkthroughId}, walkthrough).$promise.then(loadAnswers);
                } else {
                    // create
                    cwtWalkthroughService.create(walkthrough).$promise.then(loadAnswers);
                }
            };

            if ($routeParams.walkthroughId && $routeParams.walkthroughId.match(/^[0-9]+$/)) {
                cwtWalkthroughService.get({walkthroughId: $routeParams.walkthroughId}).$promise.then(loadAnswers);
            }
            
            $scope.questionSet = cwtQuestionSetService.get({questionSetId: $routeParams.questionSetId});
            
            $scope.checkAnswers = function() {
                if (answersDirty) {
                    saveAnswers();
                    answersDirty = false;
                }
            }
            $scope.$watch('answers', function(newAnswers, oldAnswers) {
                answersDirty = true;
            }, true);
        }
    }
]);