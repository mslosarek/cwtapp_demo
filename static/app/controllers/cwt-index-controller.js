cwtapp.controller('cwtIndexController', [
    '$scope', '$routeParams', 'cwtQuestionSetService', 'cwtWalkthroughService',
    function ($scope, $routeParams, cwtQuestionSetService, cwtWalkthroughService) {
        $scope.walkthroughs = cwtWalkthroughService.read();
    }
]);