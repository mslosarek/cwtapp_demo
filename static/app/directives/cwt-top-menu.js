cwtapp
    .directive('cwtTopMenu', function() {
        return {
            controller: [
                '$scope', 'cwtQuestionSetService',
                function($scope, cwtQuestionSetService) {
                    $scope.questionSets = cwtQuestionSetService.read();
                }
            ],
            templateUrl: 'app/directives/cwt-top-menu.html'
        };
    });