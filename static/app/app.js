var cwtapp = angular.module('cwtapp', [
    'ngRoute',
    'ngResource'
]);


cwtapp.config([
    '$routeProvider',
    function($routeProvider) {
        $routeProvider
            .when('/walkthrough/:questionSetId/:walkthroughId?', {
                templateUrl: 'app/views/walkthrough.html',
                controller: 'cwtWalkthroughController'
            })
            .when('/', {
                templateUrl: 'app/views/index.html',
                controller: 'cwtIndexController'
            })
            .otherwise({
                redirectTo: '/'
            });
    }
]);