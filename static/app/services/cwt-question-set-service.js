cwtapp.service('cwtQuestionSetService', [
    '$resource',
    function($resource) {
        var self = this,
            questionSetResource = $resource(
                '/api/v1/question_sets/:questionSetId', 
                {
                    questionSetId:'@id'
                },
                {
                    'read': {method: 'GET', isArray: true}
                }
            );

        self.get = questionSetResource.get;
        self.read = questionSetResource.read;
    }
])