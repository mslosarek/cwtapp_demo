cwtapp.service('cwtWalkthroughService', [
    '$resource',
    function($resource) {
        var self = this,
            walkthroughResource = $resource(
                '/api/v1/walkthroughs/:walkthroughId', 
                {
                    walkthroughId:'@id'
                },
                {
                    'create': {method: 'POST'},
                    'update': {method: 'PUT'},
                    'read': {method: 'GET', isArray: true}
                }
            );

        self.get = walkthroughResource.get;
        self.create = walkthroughResource.create;
        self.update = walkthroughResource.update;
        self.read = walkthroughResource.read;
    }
])